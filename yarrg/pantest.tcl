#!/usr/bin/wish

pkg_mkIndex .
set auto_path [concat . $auto_path]

#source panner.tcl
#namespace import panner::*

package require panner

set scale 50

canvas .c
for {set x -$scale} {$x < 500} {incr x $scale} {
    for {set y -$scale} {$y < 500} {incr y $scale} {
	.c create text $x $y -text "${x}x${y}" -anchor sw
    }
}

panner::canvas-scroll-bbox .c
panner::create .p .c 200 200 1

pack .p -side right
pack .c -expand y -fill both -side left
