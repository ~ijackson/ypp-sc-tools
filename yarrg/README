YARRG - Yet Another Revenue Research Gatherer
=============================================

Overview
--------

This tool will:
  - screenscrape the commodities trading screen
  - produce the results as a tab separated values file
  - upload the results to the YARRG and PCTB servers

To build, install the dependencies, cd to this directory and type `make'.

To screenscrape and upload to both servers, select `trade
commodities' from the hold of a vessel or building, and run:
   ./yarrg
We upload to the dedicated yarrg server http://yarrg.chiark.net/,
and also to the PCTB server http://pctb.crabdance.com/.

Or, for example, for a tab-separated values dump:
   ./yarrg --tsv >commods.tsv

While it is capturing the screenshots, do not move the mouse or use
the keyboard.  Keyboard focus must stay in the YPP client window.  If
you use Gnome or KDE you must raise the YPP client window so that it
is completely unobscured - the yarrg program tries to do this but
metacity and kwin do not honour the request.


Command-line options
--------------------

Setting the operation mode:
  --find-window-only       Just check that we can find the YPP client window.
  --screenshot-only        Page through and take screenshots, do not OCR
  --analyse-only | --same  Process previously taken screenshots
  --everything (default)   Take screenshots and process them

Options to vary the processing:
  --single-page         One screenful, no paging - results will be incomplete
  --quiet               Suppress progress messages
  --screenshot-file F   Store or read screenshots in F rather than _pages.ppm
  --window-id ID        Specified X window is the YPP client - do not search
  --edit-charset        Enable character set editing.  See README.dictionary.
  --no-edit-charset     Do not edit charset even if _local-char*.txt exists.
  --find-island         Find and print the ocean and island.  Suppresses OCR
                         and output unless used with result processing option.
  --test-servers        Set default servers to be the test servers, not
                         the real live ones (doesn't affect explicit settings).

Controlling what happens to the results - one or more:
  --upload (default)    Upload to both the YARRG and PCTB servers
  --upload-pctb         Upload to the PCTB servers
  --upload-yarrg        Upload to the YARRG servers
  --tsv                 Print data as clean tab-separated-values file
  --raw-tsv             Dump the raw (not deduped, unsorted) OCR'd data
  --best-prices         Print best buy and sell price for each commodity
  --arbitrage           Print arbitrage opportunities

Privacy options, which control conversations with the dictionary server:
  --dict-local-only  *  Do not talk to the server even to fetch new dictionary.
  --dict-read-only   *  Only fetch new dictionary, do not submit new entries.
  --dict-anon           Don't quote pirate name if submitting entries.
  --dict-submit         Submit entries quoting my pirate name.  (default)
Please do not use options marked * with --upload.  See README.privacy.

Options to override which servers we talk to:
  --yarrg-server HOST|URL When uploading to YARRG, use HOST or URL.
  --pctb-server HOST|URL  When talking to PCTB, use HOST or URL.
  --dict-submit-url URL   Submit dictionary updates here (default: use yarrg).
  --dict-update-from SRC  Fetch updated master dictionary with rsync from SRC.
Or set the environment variables
  YPPSC_YARRG{_YARRG, _PCTB, _DICT_UPDATE, _DICT_SUBMIT}


Installation requirements
-------------------------

Your X server must be 24bpp (or better).

This program has quite a few dependencies:
							Package (Debian etch)

 - For building, C compiler and build environment	build-essential
 - pnm library, including dev files for building	libnetpbm10-dev
 - pnm command line utilities for image manipulation	netpbm
 - X11 libraries, including dev files for building	libx11-dev
 - XTEST library, including dev files for building	libxtst-dev
 - Perl-compatible regexp library, including dev files  libpcre3-dev
 - Tk interpreter /usr/bin/wish				tk8.4
 - Perl module XML::Parser				libxml-parser-perl
 - Perl module JSON::Parser				libjson-perl
 - XTEST extension in the X server			(part of X package)
 - Perl interpreter and basic modules			perl (usu.installed)

On other Linux distros the packages may have different names, but
these should be roughly right for Debian and its derivatives.  You can
install them with this rune:
  sudo apt-get install git-core build-essential libnetpbm10-dev netpbm libx11-dev libxtst-dev libpcre3-dev tk8.4 libxml-parser-perl libjson-perl


The supplied helper programs
  dictionary-manager
  commod-results-processor
  database-info-fetch
must (currently) also be in the current working directory when you run
the main yarrg program.

The data files (see README.files) are also left in the current working
directory.  There is not yet any feature to have the data files and
helpers be somewhere else.


Reporting problems
------------------

If you need to report a bug, for example an inability to recognise,
please be sure to remember the exact error message and circumstances.
Also, for recognition problems there will probably be a very useful
screenshot file called `_pages.ppm'.  This is likely to be very large
so don't just email it to me, but if you can put it up on a webpage
for me to download that will help.  At least keep a copy of it.

If the problem is a failure to cope with some particular YPP client
display and is reproducible, try running:
   ./yarrg --raw-tsv --single-page
If this reproduces the problem, please email me the screenshot file
_pages.ppm, which will consist only of the single screen, plus the
error messasge.  I'll then be able to understand what's wrong,
hopefully.


Privacy
-------

The main purpose of this program is to connect to the YARRG and PCTB
servers and upload data.  It will do that if you run it with --upload.

This program will also, by default, talk to the dictionary server I
have set up: to download updated image dictionaries, and to upload new
dictionary entries which you create with the yarrg client dictionary
GUI.  This feature is mentioned in and controllable in the GUI itself,
so it won't happen without you knowing about it.

The uploads will by default mention your ocean and pirate name; if you
don't want that, pass the --dict-anon option, or untick the box in the
GUI.

See README.privacy for full details.


Disclaimers, authorship and copyright
-------------------------------------

The PCTB server is a project of Joel Lord and various others.

The YARRG server was inspired by PCTB and is a project of the crew
Special Circumstances (on the Cerulean Ocean) and of the Sinister
Greenend Organisation.

PCTB and YARRG are both completely unofficial and Three Rings, the
operators of Yohoho Puzzle Pirates, have nothing to do with either of
them.  Please refer queries to us, not to Three Rings.  If you want to
reuse the code here to do substantially novel things, you should ask
Three Rings for permission (for example, by petitioning an Ocean
Master).


This yarrg screenscraper and upload client was written entirely from
scratch by me, Ian Jackson, with assistance from Clare Boothby,
Stephen Early, and Naath.  It is part of ypp-sc-tools, a set of
third-party tools for assisting players of Yohoho Puzzle Pirates.

ypp-sc-tools and YARRG are
Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
Copyright (C) 2009 Clare Boothby
Copyright (C) 2009 Steve Early
Copyright (C) 2009 Naath Cousins

This program is free software: you can redistribute it and/or modify
it under the terms of
 (a) for the website code including the route searcher,
      the GNU Affero General Public License and
 (b) for the rest of the code, GNU General Public License
as published by the Free Software Foundation, either version 3 of
each applicable the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License and
GNU Affero General Public License along with this program.  If not,
see <http://www.gnu.org/licenses/>.

Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
are used without permission.  Once again, this program is not endorsed
or sponsored by Three Rings.


The character and UI images copied from the YPP client, and submitted
to stored and shared by the YPP SC YARRG dictionary server, are those
provided by Three Rings as part of the YPP client and by your Java
installation.  I regard the current use of these images in this way as
Fair Dealing (in the UK) or Fair Use (in the USA).

These images do not form part of the ypp-sc-tools distribution,
although the ypp-sc-tools yarrg client does download them automatically
from my dictionary server when run in the most ordinary way.


 - Ian Jackson
   ijackson@chiark.greenend.org.uk
   Aristarchus on the Cerulean ocean
