/*
 * IPC to external resolver program
 */
/*
 *  This is part of ypp-sc-tools, a set of third-party tools for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#include "convert.h"
#include "ocr.h"

static FILE *resolver;
static pid_t resolver_pid;
static int resolver_done;

FILE *resolve_start(void) {
  int jobpipe[2],donepipe[2];

  if (!o_resolver) return 0;

  if (!resolver) {
    sysassert(! pipe(jobpipe) );
    sysassert(! pipe(donepipe) );
    resolver_pid= fork();
    sysassert(resolver_pid!=-1);
    if (!resolver_pid) {
      sysassert( dup2(jobpipe[0],0) ==0 );
      sysassert(! close(jobpipe[1]) );
      sysassert(! close(donepipe[0]) );
      /* we know donepipe[1] is >= 4 and we have dealt with all the others
       * so we aren't in any danger of overwriting some other fd 4: */
      sysassert( dup2(donepipe[1],4) ==4 );
      EXECLP_HELPER("dictionary-manager",
        DEBUGP(callout)                 ? "--debug"       : "--noop-arg",
        DEBUGP(callout) && DEBUGP(rect) ? "--debug-rect"  : "--noop-arg",
		    o_quiet ? "--quiet" : "--noop-arg",
		    "--automatic-1",
		    (char*)0);
      sysassert(!"execlp dictionary-manager --automatic failed");
    }
    sysassert(! close(jobpipe[0]) );
    sysassert(! close(donepipe[1]) );
    resolver= fdopen(jobpipe[1],"w"); sysassert(resolver);
    resolver_done= donepipe[0];
  }

  progress("");
  return resolver;
}

void resolve_finish(void) {
  char cb;
  
  sysassert(!ferror(resolver));
  sysassert(!fflush(resolver));

  sysassert(resolver);

  int r;
  for (;;) {
    r= read(resolver_done,&cb,1);
    if (r==-1) { sysassert(errno==EINTR); continue; }
    break;
  }

  if (r==0) {
    waitpid_check_exitstatus(resolver_pid, "dictionary-manager", 0);
    fclose(resolver);
    close(resolver_done);
    resolver= 0;
  } else {
    assert(r==1);
    sysassert(cb==0);
  }
}
