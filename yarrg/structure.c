/*
 * Parsing of the structure of the YPP client's displayed image
 */
/*
 *  This is part of ypp-sc-tools, a set of third-party tools for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#include "structure.h"

DEBUG_DEFINE_DEBUGF(struct)

#define START_MAIN {200,200}
#define MIN_COLUMNS         6
#define INTERESTING_COLUMNS 7
#define TEXT_COLUMNS        2
#define MAX_COLUMNS         7

struct PageStruct {
  Rect mr;
  int commbasey, comminty, pagerheight;
  int colrightx[INTERESTING_COLUMNS];
};

const CanonImage *page_images[MAX_PAGES];
static PageStruct page_structs[MAX_PAGES];
const RgbImage *page0_rgbimage;
int npages;

static int text_h=-1, columns=-1;

static OcrReader *rd;

static const CanonImage *cim;
static PageStruct s;

char *archipelago, *island;

#define OTHERCOORD_x y
#define OTHERCOORD_y x


void select_page(int page) {
  cim= page_images[page];
  s= page_structs[page];
  assert(cim);
}


typedef struct {
  Rgb rgbx; /* on screen, REVERSED BYTES ie r||g||b */
  char c; /* canonical */
} CanonColourInfo;

const CanonColourInfo canoncolourinfo_table[]= {
  { 0x475A5E, '*' }, /* edge */
  { 0x2C5F7A, '*' }, /* edge just under box heading shadow */
  { 0xC5C7AE, '*' }, /* blank area of partial commodities list */
  { 0x6B828C, '*' }, /* background of ship status meter area */
  { 0x934405, '*' }, /* border of ship meter area */
  { 0x7D9094, '+' }, /* interbox */
  { 0x022158, 'O' }, /* ahoy /w output foreground */
  { 0xB5B686, 'H' }, /* ahoy /w output heading background */

  { 0xBDC5BF, ' ' }, /* background - pale  Sugar cane, etc. */
  { 0xADB5AF, ' ' }, /* background - dark                   */
  { 0xC7E1C3, ' ' }, /* background - pale  Swill, etc.      */
  { 0xB5CFB1, ' ' }, /* background - dark                   */
  { 0xD6CEB0, ' ' }, /* background - pale  Madder, etc.     */
  { 0xC8C0A2, ' ' }, /* background - dark                   */
  { 0xE0E1D3, ' ' }, /* background - pale  Lorandite, etc.  */
  { 0xD0D1C3, ' ' }, /* background - dark                   */
  { 0xE5E6C1, ' ' }, /* background - pale  Cloth            */
  { 0xD7D8B3, ' ' }, /* background - dark                   */
  { 0xEDDED9, ' ' }, /* background - pale  Dye              */
  { 0xDACBC6, ' ' }, /* background - dark                   */
  { 0xD3DEDF, ' ' }, /* background - pale  Paint            */
  { 0xC5D0D1, ' ' }, /* background - dark                   */
  { 0xDCD1CF, ' ' }, /* background - pale  Enamel           */
  { 0xCEC3C1, ' ' }, /* background - dark                   */
  { 0xF3F6F5, ' ' }, /* background - pale  fruit            */
  { 0xE2E7E5, ' ' }, /* background - dark                   */

  { 0x000000, 'o' }, /* foreground */
  { 0xD4B356, ' ' }, /* background (cursor) */
  { 0xFFFFFF, 'o' }, /* foreground (cursor) */

  { 0x5B93BF, '_' }, /* selector dropdown background */
  { 0xD7C94F, 'X' }, /* selector dropdown foreground */
  { 0,0 }
};

CanonColourInfoReds canoncolourinfo_tree;

void canon_colour_prepare(void) {
  const CanonColourInfo *cci;
  for (cci=canoncolourinfo_table; cci->c; cci++) {
    unsigned char r= cci->rgbx >> 16;
    unsigned char g= cci->rgbx >>  8;
    unsigned char b= cci->rgbx;

    CanonColourInfoGreens *greens= canoncolourinfo_tree.red2[r];
    if (!greens) {
      greens= canoncolourinfo_tree.red2[r]= mmalloc(sizeof(*greens));
      FILLZERO(*greens);
    }

    CanonColourInfoBlues *blues= greens->green2[g];
    if (!blues) {
      blues= greens->green2[g]= mmalloc(sizeof(*blues));
      memset(blues, '?', sizeof(*blues));
    }

    blues->blue2[b]= cci->c;
  }
}

static inline char get(int x, int y) { return cim->d[y * cim->w + x]; }
static inline char get_p(Point p) { return get(p.x,p.y); }


static void mustfail1(const char *file, int line, const char *what) {
  fprintf(stderr,
 "\n\n"
 "Unable to figure out contents of YPP client display.\n"
 "Please check the following:\n"
 "   * YPP client is showing commodity listing screen\n"
 "   * YPP client window is on top (we try to raise it but your window\n"
 "      manager might have prevented that from succeeding)\n"
 "\n"
 "If all of these are true, please report this as a fault.\n\n"
	  "Technical details:"
	  " %s:%d: requirement failed:\n"
	  " %s\n",
	  file, line, what);
}
static void mustfail2(void) NORET;
static void mustfail2(void) {
  fprintf(stderr, "\n\nGiving up.\n");
  exit(8);
}

#define MUST(x, ifnot) do{			\
    if (__builtin_expect(!(x), 0)) {		\
      mustfail1(__FILE__,__LINE__,#x);		\
      ifnot;					\
      mustfail2();				\
    }						\
  }while(0)

#define MP(v) fprintf(stderr," %s=%d,%d",#v,(v).x,(v).y)
#define MI(v) fprintf(stderr," %s=%d",   #v,(v))
#define MIL(v) fprintf(stderr," %s=%ld", #v,(v))
#define MRGB(v) fprintf(stderr," %s=%06"PRIx32, #v,(v))
#define MC(v) fprintf(stderr," %s='%c'", #v,(v))
#define MS(v) fprintf(stderr," %s=\"%s\"", #v,(v))
#define MF(v) fprintf(stderr," %s=%f", #v,(v))
#define MSB(v) fprintf(stderr," %s", (v))
#define MR(v) fprintf(stderr," %s=%d,%d..%d,%d",\
                      #v,(v).tl.x,(v).tl.y,(v).br.x,(v).br.y)


#define REQUIRE_RECTANGLE(tlx,tly,brx,bry,ok) \
 require_rectangle(tlx, tly, brx, bry, ok, __LINE__);

#define FOR_P_RECT(p,rr)				\
  for ((p).x=(rr).tl.x; (p).x<=(rr).br.x; (p).x++)	\
    for ((p).y=(rr).tl.y; (p).y<=(rr).br.y; (p).y++)

static void require_rectangle_r(Rect rr, const char *ok, int lineno) {
  Point p;
  FOR_P_RECT(p,rr) {
    int c= get_p(p);
    MUST( strchr(ok,c), ({
      MI(lineno),MR(rr);MP(p);MS(ok);
    }));
  }
}
static void require_rectangle(int tlx, int tly, int brx, int bry,
			      const char *ok, int lineno) {
  Rect rr= {{tlx,tly},{brx,bry}};
  require_rectangle_r(rr, ok, lineno);
}

static void debug_rect(const char *what, int whati, Rect rr) {
  if (!DEBUGP(rect)) return;
  int y,w;
  fprintf(debug, "%s %d: %d,%d..%d,%d:\n", what, whati,
	  rr.tl.x,rr.tl.y, rr.br.x,rr.br.y);
  w= rr.br.x - rr.tl.x + 1;
  for (y=rr.tl.y; y<=rr.br.y; y++) {
    fprintf(debug, "%4d%*s|", y, rr.tl.x,"");
    fwrite(cim->d + y*cim->w + rr.tl.x, 1, w, debug);
    fputc('|',debug);
    fputc('\n',debug);
  }
  debug_flush();
}

static int commod_selector_matches(Rect search, const char *const *all,
				   int allh, int allw) {
  int alloffy, alloffx;
  for (alloffy=0; alloffy < search.br.y; alloffy++) {
    if (alloffy+allh-1 < search.tl.y) continue;
    for (alloffx=search.tl.x; alloffx+allw-1 <= search.br.x; alloffx++) {
      int good=0, bad=0;
      int x,y;
      for (x=0; x<allw; x++)
	for (y=0; y<allh; y++) {
	  int want= all[y][x];
	  if (want==' ') continue;
          if (get(alloffx+x, alloffy+y) == want)
	    good++;
	  else
	    bad++;
	}
      debugf("CHECKCOMMOD alloff=%d,%d good=%d bad=%d\n",
	     alloffx,alloffy, good,bad);
      if (good > 20*bad)
	return 1;
    }
  }
  return 0;
}

#define WALK_UNTIL(point,coord,increm,last,edge)			\
  for (;;) {								\
    if ((point).coord == (last)+(increm)) break;			\
    if (get_p((point)) == (edge)) { (point).coord -= (increm); break; }	\
    (point).coord += (increm);						\
  }

#define WALK_UNTIL_MUST(point,coord,increm,last,edge)	\
  do {							\
    WALK_UNTIL(point,coord,increm,last,edge);		\
    MUST( (point).coord != (last)+(increm),		\
	  MP(point); MI(increm); MI(last); MC(edge);	\
	  );						\
  }while(0)

#define ADJUST_BOX(search,insidechrs,OP,want, lim,LIMIT_MUST, TLBR,XY,increm) \
  for (;;) {								      \
    LIMIT_MUST( (search).tl.XY != (search).br.XY &&			      \
		(search).TLBR.XY != (lim),				      \
		MR((search));MSB(#TLBR);MSB(#XY) );			      \
    int got=0;								      \
    Point p=(search).tl;						      \
    for (p.XY=(search).TLBR.XY;						      \
	 p.OTHERCOORD_##XY <= (search).br.OTHERCOORD_##XY;		      \
	 p.OTHERCOORD_##XY++)						      \
      got += !!strchr(insidechrs, get_p(p));				      \
    if ((got) OP (want))						      \
      break;								      \
    (search).TLBR.XY += increm;						      \
  }

void find_structure(const CanonImage *im,
		    PageStruct **pagestruct_r,
		    int *max_relevant_y_r,
		    Point *commod_focus_point_r,
		    Point *commod_page_point_r,
		    Point *commod_focuslast_point_r) {
  cim= im;

  FILLZERO(s);
  Rect whole = { {0,0}, {cim->w-1,cim->h-1} };

  if (DEBUGP(rect)) {
    int xscaleunit, y,x;
    for (y=0, xscaleunit=1; y<4; y++, xscaleunit*=10) {
      fprintf(debug,"     ");
      for (x=0; x<=cim->w; x++) {
	if (x % xscaleunit) fputc(' ',debug);
	else fprintf(debug,"%d",(x / xscaleunit)%10);
      }
      fputc('\n',debug);
    }
  }

  Point mainr_tl= START_MAIN;
  s.mr.tl= mainr_tl;
  WALK_UNTIL_MUST(s.mr.tl, y,-1, whole.tl.y, ' ');
  s.mr.br= s.mr.tl;

  WALK_UNTIL_MUST(s.mr.tl, x,-1, whole.tl.x, '*');
  WALK_UNTIL_MUST(s.mr.tl, y,-1, whole.tl.y, '*');
  WALK_UNTIL_MUST(s.mr.br, x,+1, whole.br.x, '*');
  WALK_UNTIL_MUST(s.mr.br, y,+1, whole.br.y, '*');

  REQUIRE_RECTANGLE(s.mr.tl.x-1, s.mr.tl.y, s.mr.tl.x-1, s.mr.br.y, "*");
  REQUIRE_RECTANGLE(s.mr.br.x+1, s.mr.tl.y, s.mr.br.x+1, s.mr.br.y, "*");
  REQUIRE_RECTANGLE(s.mr.tl.x, s.mr.tl.y-1, s.mr.br.x, s.mr.tl.y-1, "*");
  REQUIRE_RECTANGLE(s.mr.tl.x, s.mr.br.y+1, s.mr.br.x, s.mr.br.y+1, "*");

#define CHECK_STRIP_BORDER(tlbr,xy,increm)		\
  do {							\
    Point csb_p, csb_p2;				\
    Rect csb_r;						\
    csb_p= s.mr.tl;					\
    csb_p.x++; csb_p.y++;				\
    csb_p2= csb_p;					\
    csb_p2.x++; csb_p2.y++;				\
    csb_p.xy= s.mr.tlbr.xy;				\
    csb_p2.xy= s.mr.tlbr.xy;				\
    if (get_p(csb_p)=='+' &&				\
	get_p(csb_p2)=='+') {				\
      csb_r= s.mr;					\
      csb_r.tl.xy= csb_p.xy;				\
      csb_r.br.xy= csb_p.xy;				\
      require_rectangle_r(csb_r, "+", __LINE__);	\
      s.mr.tlbr.xy += increm;				\
    }							\
  } while(0)

  debug_rect("s.mr",0, s.mr);

  CHECK_STRIP_BORDER(tl,x,+1);
  CHECK_STRIP_BORDER(tl,y,+1);
  CHECK_STRIP_BORDER(br,x,-1);
  CHECK_STRIP_BORDER(br,y,-1);

  debug_rect("s.mr",1, s.mr);

  Rect updown= {START_MAIN,START_MAIN};
  const int chkw= 100;
  updown.br.x += chkw-1;
  updown.br.y++;
  debug_rect("updown",__LINE__,updown);

  ADJUST_BOX(updown, "+", >=,chkw, s.mr.tl.y,   MUST, tl,y,-1);
  debug_rect("updown",__LINE__,updown);
  updown.br.y= updown.tl.y;
  updown.tl.y= updown.tl.y-1;

  ADJUST_BOX(updown, "+*",>=,chkw, s.mr.tl.y-1, MUST, tl,y,-1);
  debug_rect("updown",__LINE__,updown);

  s.commbasey= updown.tl.y + 1;
  s.comminty= updown.br.y - updown.tl.y;

  Rect across= {{ s.mr.tl.x - 1, s.commbasey                 },
		{ s.mr.tl.x,     s.commbasey + s.comminty-2 }};
  int colno=0;
  for (;;) {

#define LIMIT_QUITEQ(cond,mp) { if (!(cond)) break; }
    debug_rect("across",colno*1000000+__LINE__, across);
    ADJUST_BOX(across, "+",>=,s.comminty-1,s.mr.br.x,LIMIT_QUITEQ,br,x,+1);
    debug_rect("across",colno*1000000+__LINE__, across);

    MUST( colno < MAX_COLUMNS,
	  MI(colno);MR(across);MR(s.mr);MI(s.commbasey); );
    int colrx= across.br.x-1;
    if (colrx >= s.mr.br.x) colrx= s.mr.br.x;
    if (colno < INTERESTING_COLUMNS)
      s.colrightx[colno]= colrx;
      
    colno++;
    
    if (across.br.x >= s.mr.br.x)
      break;

    REQUIRE_RECTANGLE(across.br.x,s.mr.tl.y, across.br.x,s.mr.br.y, "+");
    across.br.x++;
  }
  MUST( colno >= MIN_COLUMNS, MI(colno);MR(s.mr);MR(across); );

  const int pagerh= 6;
  Rect pager= {{ s.mr.br.x,     s.mr.br.y - (pagerh-1) },
	       { s.mr.br.x + 1, s.mr.br.y              }};

  debug_rect("pager",__LINE__,pager);
  ADJUST_BOX(pager, "o",>=,pagerh-2, whole.br.x,MUST, br,x,+1);
  debug_rect("pager",__LINE__,pager);

  pager.tl.x= pager.br.x;
  pager.br.x= pager.br.x + 1;
  debug_rect("pager",__LINE__,pager);
  ADJUST_BOX(pager, "o",>=,pagerh-2, whole.br.x,MUST, br,x,+1);
  debug_rect("pager",__LINE__,pager);

  ADJUST_BOX(pager, "o",>=,RECT_W(pager)-2, s.mr.tl.y,LIMIT_QUITEQ, tl,y,-1);
  debug_rect("pager",__LINE__,pager);

  pager.br.y= pager.tl.y;
  pager.tl.y= pager.tl.y-1;
  if (pager.tl.y > s.mr.tl.y)
    ADJUST_BOX(pager, "+",>,1, s.mr.tl.y,LIMIT_QUITEQ, tl,y,-1);
  debug_rect("pager",__LINE__,pager);
  s.pagerheight= pager.br.y - pager.tl.y;

#define SET_ONCE(var,val) do{						\
    int v= (val);							\
    if ((var)==-1) (var)= v;						\
    else MUST( (var) == v, MSB(#var);MI((var));MI(v);MR(s.mr); );	\
  }while(0)

  SET_ONCE(columns, colno);
  SET_ONCE(text_h, s.comminty - 1);

  if (pagestruct_r) {
    *pagestruct_r= mmalloc(sizeof(s));
    **pagestruct_r= s;
  }
  
  if (max_relevant_y_r)
    SET_ONCE(*max_relevant_y_r, s.mr.br.y + 10);

  if (commod_focus_point_r) {
    *commod_focus_point_r= s.mr.tl;
    commod_focus_point_r->x += 10;
    commod_focus_point_r->y += s.comminty/3;
  }
  if (commod_focuslast_point_r) {
    *commod_focuslast_point_r= s.mr.br;
    commod_focuslast_point_r->x -= 10;
    commod_focuslast_point_r->y -= s.comminty/3;
  }
  if (commod_page_point_r) {
    commod_page_point_r->x= (pager.tl.x + pager.br.x) / 2;
    commod_page_point_r->y=  pager.br.y - 1;
  }

  MUST( text_h <= OCR_MAX_H, MI(text_h) );
}		    

void check_correct_commodities(void) {
  Rect search;

  search.tl= s.mr.tl;  search.tl.x +=  34;  search.tl.y -= 44;
  search.br= s.mr.tl;  search.br.x += 114;  search.br.y -= 23;
  MUST(search.br.x < cim->w-1 && search.tl.y > 0,
       MR(search);MI(cim->w);MI(cim->h));
  
  debug_rect("commodselr",0, search);
  ADJUST_BOX(search,"_",>=,10, cim->h, MUST, tl,y,+1);
  ADJUST_BOX(search,"_",>=,10, 0,      MUST, br,y,-1);
  debug_rect("commodselr",1, search);

  static const char *all_small[]= {
    "   ___________________________________   ",
    "  ________X____X__X____________________  ",
    " ________ X___ X_ X_____XXXXXXXXXXX_____ ",
    "_________X_X__ X_ X______XXXXXXXXX_______",
    "________ X X__ X_ X_______XXXXXXX________",
    "________X_ _X_ X_ X________XXXXX_________",
    "_______ X__ X_ X_ X_________XXX__________",
    "_______XXXXXXX X_ X__________X___________",
    " _____ X     X X_ X______________________",
    "  ____X_____ _XX_ X______________________",
    "   __ _______  __ ______________________ ",
  };
  static const char *all_big[]= {
    "???_______________________________________???",
    "??_________________________________________??",
    "?_________X______X___X______________________?",
    "_________?X_____?X__?X______XXXXXXXXXXX______",
    "_________X_X____?X__?X_______XXXXXXXXX_______",
    "________?X?X____?X__?X________XXXXXXX________",
    "________X_?_X___?X__?X_________XXXXX_________",
    "_______?X__?X___?X__?X__________XXX__________",
    "_______?XXXXX___?X__?X___________X___________",
    "_______X????_X__?X__?X_______________________",
    "?_____?X____?X__?X__?X_______________________",
    "??____X_____?_X_?X__?X_______________________",
    "???__?_______?__?___?_______________________?",
  };
  static const char *all_fuzzy[]= {
    "???___________________________________???",
    "??_______???___X__X____________________??",
    "?_______????__?X_?X_____XXXXXXXXXXX_____?",
    "________?????_?X_?X______XXXXXXXXX_______",
    "________?????_?X_?X_______XXXXXXX________",
    "_______??????_?X_?X________XXXXX_________",
    "_______??_?????X_?X_________XXX__________",
    "______??XXXXX??X_?X__________X___________",
    "?_____?????????X_?X______________________",
    "??___???____???X_?X______________________",
    "???__??_____???__?______________________?",
  };

#define COMMOD_SELECTOR_MATCHES(all)				\
  commod_selector_matches(search, all,				\
			  sizeof((all))/sizeof((all)[0]),	\
			  strlen((all)[0]))

  if (!(COMMOD_SELECTOR_MATCHES(all_small) ||
	COMMOD_SELECTOR_MATCHES(all_big) ||
	COMMOD_SELECTOR_MATCHES(all_fuzzy)))
    fatal("Commodities selector not set to `All'.");
}

CanonImage *alloc_canon_image(int w, int h) {
  CanonImage *im= mmalloc(sizeof(CanonImage) + w*h);
  im->w= w;
  im->h= h;
  memset(im->d,'?',w*h);
  return im;
}

static void file_read_image_ppm(FILE *f) {
  struct pam inpam;
  unsigned char rgb_buf[3];
  CanonImage *im;
  RgbImage *ri;
  PageStruct *pstruct;

  progress("page %d reading       ...",npages);

  pnm_readpaminit(f, &inpam, sizeof(inpam));
  if (!(inpam.maxval == 255 &&
	inpam.bytes_per_sample == 1 &&
	inpam.format == RPPM_FORMAT))
    fatal("PNM screenshot(s) file must be 8bpp 1 byte-per-sample RGB raw");

  CANONICALISE_IMAGE(im, inpam.width, inpam.height, ri, {
    errno=0; int rr= fread_unlocked(&rgb_buf,1,3,f);
    sysassert(rr==3);
    if (rr!=3) fatal("PNM screenshot(s) file ends unexpectedly");

    rgb= rgb_buf[0] | (rgb_buf[1] << 8) | (rgb_buf[2] << 16);
  });

  sysassert(!ferror(screenshot_file));

  if (!(npages < MAX_PAGES))
    fatal("Too many images in screenshots file; max is %d.\n", MAX_PAGES);

  find_structure(im,&pstruct, 0,0,0,0);
  store_current_page(im,pstruct,ri);
  npages++;
}

void store_current_page(CanonImage *ci, PageStruct *pstruct, RgbImage *rgb) {
  assert(ci==cim);
  progress("page %d unantialiasing...",npages);
  adjust_colours(ci, rgb);
  progress("page %d storing       ...",npages);
  if (!npages) page0_rgbimage= rgb;
  else free(rgb);
  page_images[npages]= cim;
  page_structs[npages]= *pstruct;
  debugf("STORED page %d  pagerheight=%d\n", npages, pstruct->pagerheight);
  free(pstruct);
}

void read_one_screenshot(void) {
  progress("reading screenshot...");
  file_read_image_ppm(screenshot_file);
  progress_log("read screenshot.");
}

void read_screenshots(void) {
  struct stat stab;
  
  sysassert(! fstat(fileno(screenshot_file), &stab) );
  
  for (;;) {
    if (S_ISREG(stab.st_mode)) {
      long pos= ftell(screenshot_file);
      if (pos == stab.st_size) break;
    } else {
      int c= fgetc(screenshot_file);
      if (c==EOF) break;
      ungetc(c, screenshot_file);
    }
    file_read_image_ppm(screenshot_file);
  }
  sysassert(!ferror(screenshot_file));
  progress_log("read %d screenshots.",npages);

  check_pager_motion(1,npages);
  /* When we are reading screenshots, the pages file contains the
   * `determine where we're to click' page as well as the first
   * actual data page, which we have to skip.
   */
}

#define FIXPT_SHIFT 15

typedef long Fixpt;
static inline Fixpt int2fixpt(int x) { return x<<FIXPT_SHIFT; }
static inline Fixpt dbl2fixpt(double x) { return x * int2fixpt(1); }
static inline double fixpt2dbl(Fixpt x) { return x / (1.0*int2fixpt(1)); }
static inline Fixpt fixpt_mul(Fixpt a, Fixpt b) {
  return (a*b + dbl2fixpt(0.5)) / int2fixpt(1);
}
#define MFP(v) fprintf(stderr," %s=%lx=%f", #v,(v),fixpt2dbl((v)))

static Fixpt aa_bg_chan[3], aa_scale_chan[3], aa_alpha_mean_max;
static Rgb aa_background, aa_foreground;

static void find_aa_density_prep(Rgb bg, Rgb fg, int fg_extra) {
  int i;
  unsigned char fg_chan[3];

  aa_background= bg;
  aa_foreground= fg;
  aa_alpha_mean_max= fg_extra ? int2fixpt(1)-1 : int2fixpt(1);

  for (i=0; i<3; i++) {
    aa_bg_chan[i]= int2fixpt( (aa_background >> (i*8)) & 0xff );
    fg_chan[i]=                aa_foreground >> (i*8);

    aa_scale_chan[i]= 1.0 / (int2fixpt(fg_chan[i]) + fg_extra - aa_bg_chan[i])
      * dbl2fixpt(1) * dbl2fixpt(1);
  }
}

static inline Fixpt find_aa_density(const RgbImage *ri, Point p) {
  Rgb here= ri_rgb(ri, p.x, p.y);

  if (here==aa_background) return 0;

  Fixpt alpha[3], alpha_total=0;
  int i;
  for (i=0; i<3; i++) {
    unsigned char here_chan= here >> (i*8);

    Fixpt alpha_chan= fixpt_mul(int2fixpt(here_chan) - aa_bg_chan[i],
				aa_scale_chan[i]);
    alpha[i]= alpha_chan;
    alpha_total += alpha_chan;
  }

  Fixpt one_third= dbl2fixpt(1/3.0);
  Fixpt alpha_mean= fixpt_mul(alpha_total, one_third);
  
  Fixpt thresh= dbl2fixpt(1.5/AAMAXVAL);

  MUST( -thresh <= alpha_mean && alpha_mean <= aa_alpha_mean_max + thresh,
	MP(p);
	MRGB(here);MRGB(aa_background);MRGB(aa_foreground);
	MFP(aa_alpha_mean_max);MFP(thresh);
	MFP(alpha_mean); MFP(alpha[0]);MFP(alpha[1]);MFP(alpha[2]); );

  if (alpha_mean < 0)                 alpha_mean= 0;
  if (alpha_mean > aa_alpha_mean_max) alpha_mean= aa_alpha_mean_max;

  return alpha_mean;
}

static void find_commodity(int offset, Rect *rr) {
  /* rr->tl.x==-1 if offset out of range */
  rr->tl.y= s.commbasey - offset*s.comminty;
  rr->br.y= rr->tl.y + s.comminty-2;
  if (rr->tl.y < s.mr.tl.y || rr->br.y > s.mr.br.y) { rr->tl.x=-1; return; }
  
  rr->tl.x= s.mr.tl.x;
  rr->br.x= s.mr.br.x;

  if (rr->tl.y > s.mr.tl.y)
    REQUIRE_RECTANGLE(rr->tl.x,rr->tl.y-1, rr->br.x,rr->tl.y-1, "+");
  if (rr->br.y < s.mr.tl.y)
    REQUIRE_RECTANGLE(rr->tl.x,rr->br.y+1, rr->br.x,rr->br.y+1, "+");
}

static void compute_table_location(Rect commod, int colno, Rect *cell) {
  cell->tl.y= commod.tl.y;
  cell->br.y= commod.br.y;
  cell->tl.x= !colno ? commod.tl.x : s.colrightx[colno-1]+2;
  cell->br.x=                        s.colrightx[colno];
  debug_rect("cell", colno, *cell);
}

static void ocr_rectangle(Rect r, const OcrCellType ct, FILE *tsv_output) {
  OcrResultGlyph *results, *res;

  int w= r.br.x - r.tl.x + 1;
  Pixcol cols[w+1];
  int x,y;
  for (x=0; x<w; x++) {
    FILLZERO(cols[x]);
    for (y=0; y<text_h; y++) {
      Point here= { x+r.tl.x, y+r.tl.y };
      int pixel= get_p(here);
      if (pixel==' ') pixel= '0';
      MUST( pixel >= '0' && pixel <= '0'+AAMAXVAL,
	    MC(pixel);MP(here);MSB(ocr_celltype_name(ct));MR(r); );
      pixcol_p_add(&cols[x], y, pixel-'0');
    }
  }
  FILLZERO(cols[w]);

  results= ocr(rd,ct,w,cols);
  for (res=results; res->s; res++)
    fputs(res->s,tsv_output);
}

#define FOR_COMMODITY_CELL(ROW_START, CELL, ROW_END) do{	\
    Rect rowr, cell;						\
    int tryrect, colno;						\
								\
    for (tryrect= +cim->h; tryrect >= -cim->h; tryrect--) {	\
      find_commodity(tryrect, &rowr);				\
      if (rowr.tl.x < 0)					\
	continue;						\
      debug_rect("commod",tryrect, rowr);			\
								\
      ROW_START;						\
								\
      for (colno=0; colno<columns; colno++) {			\
	compute_table_location(rowr,colno,&cell);		\
								\
	CELL;							\
      }								\
								\
      ROW_END;							\
    }								\
  }while(0);

static void adjust_colours_cell(CanonImage *ci, const RgbImage *ri,
				int colno, Rect cell) {
  Rgb background;
  unsigned char chanbg[3];
  long bg_count=0, light_count=0, dark_count=0;
  int i;
  Point p;

  background= ri_rgb(ri, cell.br.x, cell.br.y);
  for (i=0; i<3; i++)
    chanbg[i]= background >> (i*8);

  FOR_P_RECT(p,cell) {
    Rgb herergb= ri_rgb(ri, p.x, p.y);
    if (herergb==background) {
      bg_count+=3;
    } else {
      for (i=0; i<3; i++) {
	unsigned char here= herergb >> (i*8);
	if (here == chanbg[i]) bg_count++;
	else if (here < chanbg[i]) dark_count  += (chanbg[i] - here)/4 + 1;
	else if (here > chanbg[i]) light_count += (here - chanbg[i])/4 + 1;
      }
    }
  }
  long total_count= RECT_W(cell) * RECT_H(cell) * 3;

  MUST( bg_count > total_count / 2,
	MR(cell);MIL(total_count);MIL(bg_count);
	MIL(light_count);MIL(dark_count) );

  if (bg_count == total_count)
    return;

  Rgb foreground;
  double fg_extra;

  if (light_count/16 > dark_count) {
    foreground= 0xffffffU;
    fg_extra= +1;
  } else if (dark_count/16 > light_count) {
    foreground= 0;
    fg_extra= -1;
  } else {
    MUST( !"tell light from dark",
	  MR(cell);MIL(total_count);MIL(bg_count);
	  MIL(light_count);MIL(dark_count);MRGB(background); );
  }

  debugf("TABLEENTRY col=%d %d,%d..%d,%d bg=%ld light=%ld dark=%ld\n",
	 colno, cell.tl.x,cell.tl.y, cell.br.x,cell.br.y,
	 bg_count, light_count, dark_count);

  int monochrome= 1;

  find_aa_density_prep(background, foreground, fg_extra);

  FOR_P_RECT(p,cell) {
    Fixpt alpha= find_aa_density(ri,p);

    int here_int= alpha >> (FIXPT_SHIFT - AADEPTH);
    assert(here_int <= AAMAXVAL);
    if (!(here_int==0 || here_int==AAMAXVAL)) monochrome=0;
    ci->d[p.y * ci->w + p.x]= '0' + here_int;
  }

  debug_rect("cell0M", colno, cell);

  require_rectangle_r(cell, "0123456789", __LINE__);
}

void adjust_colours(CanonImage *ci, const RgbImage *ri) {
  if (!(o_mode & mf_analyse))
    return;

  cim= ci;

  FOR_COMMODITY_CELL({},({
    adjust_colours_cell(ci,ri,colno,cell);
  }),{});
}

void analyse(FILE *tsv_output) {
  int page;

  for (page=0; page<npages; page++) {
    select_page(page);

    if (!page)
      check_correct_commodities();

    if (!rd)
      rd= ocr_init(text_h);

    progress("Processing page %d...",page);

    const char *tab= "";
    
    FOR_COMMODITY_CELL({
      tab= "";
    },{
      fputs(tab, tsv_output);
      ocr_rectangle(cell,
		    colno<TEXT_COLUMNS
		    ? &ocr_celltype_text
		    : &ocr_celltype_number,
		    tsv_output);
      tab= "\t";
    },{
      fputs("\n", tsv_output);
      sysassert(!ferror(tsv_output));
      sysassert(!fflush(tsv_output));
    })
  }
  progress("Commodity table scan complete.");
}

//static Rect islandnamer;

DEBUG_DEFINE_SOME_DEBUGF(structcolon,colondebugf)

Rect find_sunshine_widget(void) {
  Rect sunshiner;

  sunshiner.tl.x= cim->w - 1034 +  885;
  sunshiner.br.x= cim->w - 1034 + 1020;
  sunshiner.tl.y= 227;
  sunshiner.br.y= 228;

  ADJUST_BOX(sunshiner,"o*",>=,30, 100,MUST, tl,y,-1);
  ADJUST_BOX(sunshiner,"o*",>=,30, 100,MUST, br,y,+1);
  debug_rect("sunshiner",0, sunshiner);

  MUST(sunshiner.br.y - sunshiner.tl.y > 20, MR(sunshiner));
  sunshiner.br.y--;

  ADJUST_BOX(sunshiner,"o",>=,20, (cim->w - 1034 + 700), MUST, tl,x,-1);
  ADJUST_BOX(sunshiner,"o",>=,20,  cim->w,               MUST, br,x,+1);
  debug_rect("sunshiner",1, sunshiner);
  return sunshiner;
}

void find_islandname(void) {
  const RgbImage *rgbsrc= page0_rgbimage;
  select_page(0);

  RgbImage *ri= alloc_rgb_image(rgbsrc->w, rgbsrc->h);
  memcpy(ri->data, rgbsrc->data, ri->w * ri->h * 3);
  
  Rect sunshiner= find_sunshine_widget();
  char sunshine[MAXIMGIDENT], archisland[MAXIMGIDENT];

  const Rgb *srcp;
  Rgb *destp, *endp;
  for (srcp= rgbsrc->data, destp=ri->data,
	 endp= ri->data + ri->w * ri->h;
       destp < endp;
       srcp++, destp++) {
    Rgb new= *srcp & 0xf0f0f0;
    *destp= new | (new>>4);
  }

  identify_rgbimage(ri, sunshiner, sunshine, "sunshine widget");
  
  if (!memcmp(sunshine,"Vessel ",5)) {
    Rect islandnamer;
    
    islandnamer.tl.x= cim->w - 1034 +  885;
    islandnamer.br.x= cim->w - 1034 + 1020;
    islandnamer.tl.y=                 128;
    islandnamer.br.y=                 156;

    ADJUST_BOX(islandnamer,"o",>=,5, 0,      MUST, tl,y,+1);
    ADJUST_BOX(islandnamer,"o",>=,5, cim->h, MUST, br,y,-1);

    ADJUST_BOX(islandnamer,"o",>=,1, 0,      MUST, tl,x,+1);
    ADJUST_BOX(islandnamer,"o",>=,1, cim->w, MUST, br,x,-1);

    debug_rect("islandnamer",0, islandnamer);
//    int larger_islandnamebry= islandnamer.tl.y + 25;
//    MUST(islandnamer.br.y < larger_islandnamebry,
//	 MR(islandnamer);MI(larger_islandnamebry));
//    islandnamer.br.y = larger_islandnamebry;
    debug_rect("islandnamer",1, islandnamer);

    int x,y;
    for (x=islandnamer.tl.x; x<=islandnamer.br.x; x++)
      for (y=islandnamer.tl.y; y<=islandnamer.br.y; y++) {
	if ((ri_rgb(ri,x,y) & 0xff) < 0x40) {
	  *RI_PIXEL32(ri,x,y)= 0;
	}
      }

    identify_rgbimage(ri, islandnamer, archisland, "island");
  } else if (!strcmp(sunshine,"Land - Ahoy!")) {
    Rect islandnamer;

    islandnamer.tl.x= (sunshiner.tl.x + sunshiner.br.x) / 2;
    islandnamer.tl.y= sunshiner.tl.y + 100;
    islandnamer.br= islandnamer.tl;
    debug_rect("islandnamer",__LINE__, islandnamer);
    
    WALK_UNTIL_MUST(islandnamer.tl,y, -1, sunshiner.br.y, 'H');
    WALK_UNTIL_MUST(islandnamer.tl,x, -1, 0,              'o');
    WALK_UNTIL_MUST(islandnamer.br,x, +1, cim->w,         'o');
    debug_rect("islandnamer",__LINE__, islandnamer);

#define RW (RECT_W(islandnamer))
#define RH (RECT_H(islandnamer))

    ADJUST_BOX(islandnamer,"O",>=,RW-4, cim->h, MUST,br,y,+1);
    debug_rect("islandnamer",__LINE__, islandnamer);

    islandnamer.br.y += 2;

    ADJUST_BOX(islandnamer,"*",<,RW, cim->h, MUST,br,y,+1);
    debug_rect("islandnamer",__LINE__, islandnamer);

    islandnamer.tl.y= islandnamer.br.y-1;
    islandnamer.br.y= islandnamer.br.y+1;
    debug_rect("islandnamer",__LINE__, islandnamer);

    ADJUST_BOX(islandnamer,"*",>=,RW, cim->h, MUST,br,y,+1);
    debug_rect("islandnamer",__LINE__, islandnamer);

    ADJUST_BOX(islandnamer,"*",<, RH, cim->w, MUST,tl,x,+1);
    debug_rect("islandnamer",__LINE__, islandnamer);

    MUST( RECT_H(islandnamer) <= 30, MR(islandnamer));

    Point p;
    int nspaces=1, might_be_colon=0;
    uint32_t colon_pattern= 0;
    p.y=-1;

    for (p.x=islandnamer.br.x; p.x>islandnamer.tl.x; p.x--) {
      colondebugf("structcolon: x=%4d nsp=%2d mbc=%d cp=%08"PRIx32" ",
		  p.x, nspaces, might_be_colon, colon_pattern);

      uint32_t pattern=0;
      int runs[32], nruns=0;
      runs[0]=0; runs[1]=0;

      find_aa_density_prep(0xCCCCAA,0x002255,0);
      
      for (p.y=islandnamer.tl.y; p.y<=islandnamer.br.y; p.y++) {
	pattern <<= 1;
	Fixpt alpha= find_aa_density(ri,p);
	if (alpha >= dbl2fixpt(0.49)) {
	   runs[nruns]++;
	   pattern |= 1u;
	} else {
	  if (runs[nruns]) {
	    nruns++;
	    runs[nruns]=0;
	  }
	}
      }

      colondebugf(" pat=%08"PRIx32" nruns=%d runs[]={%d,%d..} ",
		  pattern, nruns, runs[0],runs[1]);

      if (!pattern) {
	if (might_be_colon)
	  /* omg it _is_ a colon */
	  goto colon_found;
	nspaces++;
	might_be_colon=0;
      } else {
	if (nruns==2 && runs[1]==runs[0]) {
	  if (!nspaces) {
	    if (pattern==colon_pattern)
	      goto ok_might_be_colon;
	  } else if (nspaces>=2) {
	    colon_pattern= pattern;
	    might_be_colon=1;
	    goto ok_might_be_colon;
	  }
	} else if (nruns==1 && runs[0]==1 && might_be_colon) {
	  goto colon_found;
	}
	might_be_colon=0;
      ok_might_be_colon:
	nspaces= 0;
      }
      colondebugf(" nsp=%2d mbc=%d\n", nspaces, might_be_colon);
    }
    MUST(!"colon found", MP(p);MR(islandnamer) );

  colon_found:
    colondebugf(" found\n");
    islandnamer.br.x= p.x;

    identify_rgbimage(ri, islandnamer, archisland, "island");
  } else {

    MUST(!"sunshine shows ship or ahoy", MS(sunshine) );

  }

  char *delim= strstr(archisland," - ");
  assert(delim);
  archipelago= masprintf("%.*s", (int)(delim-archisland), archisland);
  island= masprintf("%s", delim+3);

}

void check_pager_motion(int first, int stop) {
  /* We check that the pager moved by an appropriate amount.
   * The last gap can be smaller but not bigger.
   */
  int count= stop-first;

#define PH(p) (page_structs[(p)].pagerheight)

  debugf("CHECK_PAGER_MOTION %d..%d count=%d\n", first,stop,count);

  if (count <= 1) return; /* only one page */

  double firstheight= PH(first);
  double max= count>2 ? firstheight / (count-2) : 1e6;
  double min=           firstheight / (count-1);
  max *= 1.1;
  min /= 1.1;
  max += 1.0;
  min -= 1.0;
  debugf("CHECK_PAGER_MOTION min=%g max=%g\n", min,max);
  assert(max>min);

  int skips=0, firstskip=1;
  int stops=0, firststop=1;

#define REPORT(skipstop,msg) do{			\
      skipstop##s++;					\
      if (first##skipstop<0) first##skipstop= page;	\
      if (skipstop##s<5)				\
	fprintf(stderr,msg " (page %d)\n",page);	\
    }while(0)

  int page;
  for (page=first+1; page<stop; page++) {
    int gap= PH(page-1) - PH(page);
    debugf("CHECK_PAGER_MOTION  page=%2d gap=%2d\n", page, gap);
    if (gap>max)
      REPORT(skip, "scrollbar motion probable page skip detected!");
    if (gap<min && page<stop-1)
      REPORT(stop, "scrollbar short motion gives lie to our ideas!");
  }
  MUST(!skips && !stops,
       MI(skips);MI(firstskip);MF(max);MI(PH(firstskip));MI(PH(firstskip-1));
       MI(stops);MI(firststop);MF(min);MI(PH(firststop));MI(PH(firststop-1));
       MF(firstheight);MI(PH(stop-2));MI(PH(stop-1));
       MSB(
"\n\n"
"Your YPP client seems to be paging irregularly.  Perhaps your computer\n"
"is too slow or overloaded or perhaps you are messing with the mouse ?\n"
       ));
}
