# -*- fundamental -*-

vessels
#|   Ship Name    |Gun Size|Volume | Mass  |
 |Sloop           |small   |20,250 |13,500 |
 |----------------+--------+-------+-------|
 |Cutter          |small   |60,750 |40,500 |
 |----------------+--------+-------+-------|
 |Dhow            |medium  |20,250 |13,500 |
 |----------------+--------+-------+-------|
 |Fanchuan        |large   |20,250 |13,500 |
 |----------------+--------+-------+-------|
 |Longship        |small   |20,250 |13,500 |
 |----------------+--------+-------+-------|
 |Baghlah         |medium  |27,000 |18,000 |
 |----------------+--------+-------+-------|
 |Merchant brig   |medium  |135,000|90,000 |
 |----------------+--------+-------+-------|
 |War brig        |medium  |81,000 |54,000 |
 |----------------+--------+-------+-------|
 |Merchant galleon|large   |405,000|270,000|
 |----------------+--------+-------+-------|
 |Xebec           |medium  |182,250|121,500|
 |----------------+--------+-------+-------|
 |War frigate     |large   |324,000|216,000|
 |----------------+--------+-------+-------|
 |Grand frigate   |large   |810,000|540,000|
# From http://yppedia.puzzlepirates.com/Ship; when updating,
# delete unused columns and check heading is the same as above.
# If fields reordered must change parser in Commods.pm.

shot
 small	2
 medium	3
 large	4

commodclasses
 *basic_commodities
 *ship_supplies
 *herbs
 *minerals
 *cloth
 *dye
 *paint
 *enamel
 *forageables

# Commodities are defined in `commods` sections
# Each line is
#  <name> <facts>
# and the <facts> can be
#  <mass>kg
#  <volume>l
#  *<class> (see commodclasses, above)
#  @<sort-number>    sort order value, for where in the commods list it goes
#  @<sort-number>+   sort order value; 10x line number is added
# The <name> can contain `%<reference>` which is then defined in
# a `%<reference>` section.
# Volume defaults to 1l.

commods
 kraken's blood		1kg		*dye			@105
 %d dye			1kg		*dye			@0
 %enamel enamel		5kg		*enamel			@0
 %c paint		1200g 1l	*paint			@0

 %c cloth		700g		*cloth			@2
 fine %c cloth		700g		*cloth			@5
 sail cloth		700g		*cloth			@150000

%d
 red								@100
 yellow								@110
 blue								@120
 green								@130
 lime
 navy

%enamel
 %enamelc							@0
 %newc								@10000

%c
 %oldc								@0
 %newc								@10000

# enamel colours are in a different order to everything else
%enamelc
 red								@100000+
 orange								@100000+
 yellow								@100000+
 green								@100000+
 blue								@100000+
 purple								@100000+
 white								@100000+
 black								@100000+
 tan								@100000+
 grey								@100000+
 pink								@100000+
 violet								@100000+
 navy								@100000+
 aqua								@100000+
 lime								@100000+

%oldc
 red								@100000+
 tan								@100000+
 white								@100000+
 black								@100000+
 grey								@100000+
 yellow								@100000+
 pink								@100000+
 violet								@100000+
 purple								@100000+
 navy								@100000+
 blue								@100000+
 aqua								@100000+
 lime								@100000+
 green								@100000+
 orange								@100000+

%newc
 maroon								@100000+
 brown								@100000+
 gold								@100000+
 rose								@100000+
 lavender							@100000+
 mint								@100000+
 light green							@100000+
# the following come after sailcloth in the cloth list, hence the higher number:
 magenta							@200000+
 lemon								@200000+
 peach								@200000+
 light blue							@200000+
 persimmon							@200000+

commods
 %g gems		10kg	!g	*forageables		@0
 diamonds		10kg	!g	*forageables		@200000+
 emeralds		10kg	!g	*forageables		@200000+
 moonstones		10kg	!g	*forageables		@200000+
 opals			10kg	!g	*forageables		@200000+
 pearls			10kg	!g	*forageables		@200000+
 rubies			10kg	!g	*forageables		@200000+
 sapphires		10kg	!g	*forageables		@200000+
 topazes		10kg	!g	*forageables		@200000+

%g
 amber								@200000+
 amethyst							@200000+
 beryl								@200000+
 coral								@200000+
 jade								@200000+
 jasper								@200000+
 jet								@200000+
 lapis lazuli							@200000+
 quartz								@200000+
 tigereye							@200000+
 topaz								@200000+

commods
 swill			1kg		*ship_supplies		@0+
 grog			1kg		*ship_supplies		@0+
 fine rum		1kg		*ship_supplies		@0+
 rum spice		800g		*ship_supplies		@0+
 small cannon balls	7100g		*ship_supplies		@0+
 medium cannon balls	14200g 2l	*ship_supplies		@0+
 large cannon balls	21300g 3l	*ship_supplies		@0+
 lifeboats		25kg 100l	*ship_supplies		@0+

 madder			400g		*herbs			@0+
 old man's beard	800g		*herbs			@0+
 yarrow			200g		*herbs			@0+
 sassafras		500g		*herbs			@0+
 iris root		300g		*herbs			@0+
 weld			300g		*herbs			@0+
 broom flower		200g		*herbs			@0+
 lobelia		200g		*herbs			@0+
 pokeweed berries	300g		*herbs			@0+
 indigo			700g		*herbs			@0+
 elderberries		700g		*herbs			@0+
 cowslip		700g		*herbs			@0+
 lily of the valley	300g		*herbs			@0+
 nettle			300g		*herbs			@0+
 butterfly weed		100g		*herbs			@0+
 allspice		800g		*herbs			@0+

 bananas		125kg 100l	*forageables		@300000+
 carambolas		125kg 100l	*forageables		@300000+
 coconuts		125kg 100l	*forageables		@300000+
 durians		125kg 100l	*forageables		@300000+
 limes			125kg 100l	*forageables		@300000+
 mangos			125kg 100l	*forageables		@300000+
 passion fruit		125kg 100l	*forageables		@300000+
 pineapples		125kg 100l	*forageables		@300000+
 pomegranates		125kg 100l	*forageables		@300000+
 rambutan		125kg 100l	*forageables		@300000+

 lorandite		5500g		*minerals		@0+
 leushite		4400g		*minerals		@0+
 tellurium		6200g		*minerals		@0+
 thorianite		100g		*minerals		@0+
 chalcocite		5700g		*minerals		@0+
 cubanite		4700g		*minerals		@0+
 serandite		3400g		*minerals		@0+
 papagoite		3300g		*minerals		@0+
 sincosite		3000g		*minerals		@0+
 masuyite		5100g		*minerals		@0+
 gold nuggets		400g		*minerals		@0+

 sugar cane		50kg 100l	*basic_commodities	@110
 hemp			125kg 250l	*basic_commodities	@120
 iron			7800g		*basic_commodities	@130
 wood			175kg 250l	*basic_commodities	@140
 stone			2600g		*basic_commodities	@150
 hemp oil		1kg		*basic_commodities	@160
 varnish		1kg		*basic_commodities	@180
 lacquer		1kg		*basic_commodities	@190
 kraken's ink		100g 1l		*basic_commodities	@200


client ypp-sc-tools yarrg
 lastpage

client jpctb greenend
 bug-094

#---------- OCEANS ----------
# subscriber oceans

ocean Cerulean
 Garnet
  Jubilee Island

# doubloon oceans

ocean Emerald
 Crab
  The Beaufort Islands
 Osprey
  Scurvy Reef
  Gauntlet Island
 Pleiades
  Morgana Island

ocean Meridian
 Draco
  Cetus Island
  Threewood Island
  Wyvern Island
 Basilisk
  Zechstein Island
 Komodo
  Buyan's Vortice

# International oceans (doubloon oceans)

#ocean Jade
# Cigüeña
#  Isla Scrimshaw
# Ibis
#  Isla Kiwara
# Águila
#  Cayo Escorbuto

#ocean Opal
# Canis
#  Atchafalaya-Insel

# Test ocean

ocean Ice
 Vilya
  Winking Wall Island

ocean Obsidian
 Ye Bloody Bounding Main
  Loggerhead Island
  Melanaster Island
  Picklepine Ridge
  Woodtick Island
